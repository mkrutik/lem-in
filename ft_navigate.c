/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_navigate.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 09:46:14 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 10:54:48 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_print_step(int n, char *name)
{
	ft_putstr("L");
	ft_putnbr(n);
	ft_putstr("-");
	ft_putstr(name);
	ft_putstr(" ");
}

void	ft_is_next_room_empty(t_data *src, int way, char **name, int n)
{
	t_stroke	*head;
	t_list_r	*p;
	char		*tmp;

	tmp = *name;
	head = src->way;
	while (head)
	{
		(head->f == way) ? (p = head->head) : 0;
		head = head->next;
	}
	while (p)
	{
		if (!ft_strcmp(p->name, tmp))
		{
			if (ft_is_room_loked(src->list_room, p->next->name))
				return ;
			*name = p->next->name;
			ft_lock_or_unlock(src->list_room, p->name, p->next->name);
			ft_print_step(n, p->next->name);
			return ;
		}
		p = p->next;
	}
}

void	ft_try_move_from_start(t_data *src, int *way, char **name, int n)
{
	t_stroke	*point;

	point = src->way;
	while (point)
	{
		if (!ft_is_room_loked(src->list_room, point->head->name))
		{
			*name = point->head->name;
			*way = point->f;
			ft_lock_or_unlock(src->list_room, NULL, point->head->name);
			ft_print_step(n, point->head->name);
			return ;
		}
		point = point->next;
	}
}

void	ft_navigate(t_data *src)
{
	int		i;

	ft_putstr("\n");
	while (ft_check_ant(src->list_ant, src))
	{
		i = 0;
		while (i < src->ant)
		{
			if (src->list_ant[i]->home != NULL)
			{
				if (ft_strcmp(src->n2, src->list_ant[i]->home))
					ft_is_next_room_empty(src, src->list_ant[i]->way,
							&src->list_ant[i]->home, src->list_ant[i]->n);
			}
			else
				ft_try_move_from_start(src, &src->list_ant[i]->way,
						&src->list_ant[i]->home, src->list_ant[i]->n);
			i++;
		}
		write(1, "\n", 1);
	}
}
