/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del_dublicate_link.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 12:34:57 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 09:57:24 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_del_list(t_list_link *src)
{
	t_list_link *prev;

	while (src)
	{
		prev = src;
		src = src->next;
		free(prev->name1);
		free(prev->name2);
		free(prev);
	}
}

void	ft_del_link(t_list_link *head, char *n1, char *n2, t_list_link *prev)
{
	t_list_link	*del;
	int			f;

	del = NULL;
	while (head)
	{
		if ((ft_strcmp(head->name1, n1) == 0 && ft_strcmp(head->name2, n2)
			== 0) || (ft_strcmp(head->name1, n2) == 0 &&
				ft_strcmp(head->name2, n1) == 0))
		{
			prev->next = head->next;
			f = (del == NULL) ? 1 : 0;
			(f == 1) ? (del = head) : 0;
			(f != 1) ? (head->next = del) : 0;
			(f == 1) ? (del->next = NULL) : 0;
			(f != 1) ? (del = head) : 0;
			head = prev->next;
		}
		else
		{
			prev = head;
			head = head->next;
		}
	}
	(del != NULL) ? ft_del_list(del) : 0;
}

void	ft_find_dublicate_link(t_list_link *head, int len, int i)
{
	t_list_link	*point;
	t_list_link	*prev;
	char		*name1;
	char		*name2;
	int			n;

	while (len)
	{
		point = head;
		n = i++;
		while (n-- != 0 && point->next)
			point = point->next;
		name1 = point->name1;
		name2 = point->name2;
		prev = point;
		point = point->next;
		(point) ? ft_del_link(point, name1, name2, prev) : 0;
		len--;
	}
}
