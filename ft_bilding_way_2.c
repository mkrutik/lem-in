/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bilding_way_2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 11:08:16 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 11:44:38 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void		ft_bild_first_room(t_list_r *point, t_data *data, char *name)
{
	t_list_r	*tmp;
	int			i;

	i = 1;
	tmp = ft_find_room(data->list_room, NULL, 1);
	name = ft_find_link(data->list_link, tmp->name, i);
	((data->n1 = ft_strdup(tmp->name)) == NULL) ? ft_error(2) : 0;
	tmp = ft_find_room(data->list_room, name, 0);
	((point->name = ft_strdup(tmp->name)) == NULL) ? ft_error(2) : 0;
	point->x = tmp->x;
	point->y = tmp->y;
	point->start_end = tmp->start_end;
	point->next = NULL;
	point->locked = 0;
	while ((name = ft_find_link(data->list_link, data->n1, ++i)) != NULL)
		ft_new_empty_way(data, name);
}

t_list_r	*ft_bild_another_way(t_stroke *src, t_list_r *point, t_data *data)
{
	int			i;
	t_list_r	*tmp;
	char		*n;

	i = 1;
	(point->start_end == 2) ? (data->n2 = point->name) : 0;
	if (point->start_end == 2)
		return (NULL);
	n = ft_find_link(data->list_link, point->name, i);
	while (ft_is_origin_r(src->head, n) == 1 || ft_strcmp(data->n1, n) == 0)
		if ((n = ft_find_link(data->list_link, point->name, ++i)) == NULL)
			return (NULL);
	tmp = ft_find_room(data->list_room, n, 0);
	point->next = ft_copy_room(tmp);
	while ((n = ft_find_link(data->list_link, point->name, ++i)) != NULL)
		if (ft_strcmp(n, data->n1) && !ft_is_origin_r(src->head, n))
			ft_add_new_way(data, src->head, n);
	point = point->next;
	return (point);
}
