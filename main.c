/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 10:00:04 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 12:17:38 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_r(t_data *new, char *tmp)
{
	char	*del;
	char	*res;

	del = NULL;
	res = NULL;
	while (get_next_line(0, &tmp) > 0)
	{
		if (tmp[0] == '\0')
			ft_error(8);
		if (res == NULL)
			res = ft_strjoin(tmp, "\n");
		else
		{
			del = ft_strjoin(tmp, "\n");
			free(tmp);
			tmp = res;
			res = ft_strjoin(tmp, del);
			free(del);
		}
		free(tmp);
	}
	(res == NULL || res[0] == '\0') ? ft_error(1) : 0;
	new->map = ft_strsplit(res, '\n');
	free(res);
}

void	ft_malloc_struct(t_data *new)
{
	t_list_r	*head;
	t_list_link	*h;

	new->ant = 0;
	new->room = 0;
	new->start = 0;
	new->end = 0;
	new->links = 0;
	new->map = NULL;
	head = (t_list_r*)malloc(sizeof(t_list_r));
	head->name = NULL;
	head->next = NULL;
	new->list_room = head;
	h = (t_list_link*)malloc(sizeof(t_list_link));
	h->name1 = NULL;
	new->list_link = h;
}

int		main(int argc, char **argv)
{
	t_data	*new;
	int		i;

	((new = (t_data*)malloc(sizeof(t_data))) == NULL) ? ft_error(6) : 0;
	new->color = (argc == 2 && !ft_strcmp(argv[1], "-c")) ? 1 : 0;
	ft_malloc_struct(new);
	ft_r(new, NULL);
	ft_valid(new, 0);
	ft_bild_tre(new);
	i = 0;
	(new->color == 1) ? ft_putstr("\x1b[32m") : 0;
	while (new->map[i])
	{
		ft_putstr(new->map[i]);
		ft_putstr("\n");
		i++;
	}
	ft_navigate(new);
	ft_putstr("\x1b[0m");
	return (0);
}
