/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_dublicate_coordinates.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 13:07:41 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 10:00:52 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_find_dublicate_coordinates(t_list_r *head, unsigned int len, int i)
{
	t_list_r	*point;
	t_list_r	*prev;
	ssize_t		s;
	int			n;

	s = len;
	while (s)
	{
		point = head;
		n = i++;
		while (n-- && point->next)
			point = point->next;
		prev = point;
		point = point->next;
		while (point)
			((point->x == prev->x && point->y == prev->y) ||
			ft_strcmp(point->name, prev->name) == 0) ? ft_error(9) :
				(point = point->next);
		s--;
	}
}
