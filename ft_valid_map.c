/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_valid_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 15:11:13 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 09:54:42 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

int		ft_next_dash(char *src, int end, int n)
{
	while (n--)
	{
		(src[end] == '-') ? (end++) : 0;
		while (src[end] != '-' && src[end] != '\0')
			end++;
		(n > 1 && src[end] != '\0') ? (end++) : 0;
	}
	return (end);
}

void	ft_valid_link(char *src, int i, t_data *d, int end)
{
	char	*tmp;
	int		name;
	int		x;
	int		n;

	name = 0;
	x = 1;
	while (src[end])
	{
		end = i;
		n = x;
		end = ft_next_dash(src, end, n);
		(end != 0 && (tmp = ft_strsub(src, i, (end - i))) == NULL) ? ft_error(2)
			: 0;
		(ft_find_name_in_list(d->list_room, tmp) == 1) ? (n = 5) : (x++);
		(n == 5) ? (i = ++end) : 0;
		(n == 5) ? (name++) : 0;
		(n == 5) ? (x = 1) : 0;
		(n == 5 && name == 1 && (d->n1 = ft_strdup(tmp)) == NULL) ? ft_error(2)
			: 0;
		(n == 5 && name == 2 && (d->n2 = ft_strdup(tmp)) == NULL) ? ft_error(2)
			: 0;
		free(tmp);
	}
	(name != 2) ? ft_error(8) : ft_create_link(d->list_link, d->n1, d->n2);
}

int		ft_is_command(t_data *src, int i)
{
	if (!ft_strcmp(src->map[i], "##start"))
	{
		ft_valid_room(src->map[i + 1], 0, 0, 0);
		ft_create_list_room(src->list_room, src->map[i + 1], 1);
		src->start++;
		src->room++;
		i++;
	}
	else if (!ft_strcmp(src->map[i], "##end"))
	{
		ft_valid_room(src->map[i + 1], 0, 0, 0);
		ft_create_list_room(src->list_room, src->map[i + 1], 2);
		src->end++;
		src->room++;
		i++;
	}
	i++;
	return (i);
}

int		ft_room_or_link(t_data *src, int i)
{
	if (ft_is_link(src->map[i], 0, 0, 0) == 1)
	{
		ft_valid_link(src->map[i], 0, src, 0);
		i++;
		src->links++;
	}
	else
	{
		(src->links == 0) ? 0 : ft_error(8);
		(ft_valid_room(src->map[i], 0, 0, 0) != 1) ? ft_error(1) : 0;
		ft_create_list_room(src->list_room, src->map[i], 0);
		src->room++;
		i++;
	}
	return (i);
}

void	ft_valid(t_data *src, int i)
{
	while (src->map[i] != NULL)
	{
		if (i == 0)
			(ft_valid_first_line(src->map[0]) != 1) ? ft_error(3) : (src->ant =
					ft_atoi_ssize(src->map[i++]));
		else if (src->map[i][0] == '#' && src->map[i][0] == '#')
			i = ft_is_command(src, i);
		else if (src->map[i][0] == '#' && src->map[i][1] != '#')
			i++;
		else
			i = ft_room_or_link(src, i);
	}
	(src->start == 0 || src->start > 1) ? ft_error(6) : 0;
	(src->end == 0 || src->end > 1) ? ft_error(7) : 0;
	(src->links == 0) ? ft_error(5) : 0;
	(src->room == 0) ? ft_error(4) : 0;
}
