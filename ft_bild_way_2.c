/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bild_way_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 09:40:19 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 10:26:40 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

t_list_r	*ft_find_room(t_list_r *head, char *name, int n)
{
	t_list_r	*point;

	point = head;
	while (point)
	{
		if (n == 1 && point->start_end == 1)
			return (point);
		else if (n == 2 && point->start_end == 2)
			return (point);
		else if (n == 0 && !ft_strcmp(point->name, name))
			return (point);
		point = point->next;
	}
	return (NULL);
}

int			ft_count_rooms(t_data *src, char *name)
{
	int			i;
	t_list_link	*point;

	point = src->list_link;
	i = 0;
	while (point)
	{
		if (ft_strcmp(point->name1, name) == 0 || ft_strcmp(point->name2, name)
				== 0)
			i++;
		point = point->next;
	}
	return (i);
}

char		*ft_find_link(t_list_link *head, char *name, int i)
{
	t_list_link	*find;

	find = head;
	while (find)
	{
		if (ft_strcmp(find->name1, name) == 0)
		{
			i--;
			if (i == 0)
				return (find->name2);
		}
		else if (ft_strcmp(find->name2, name) == 0)
		{
			i--;
			if (i == 0)
				return (find->name1);
		}
		find = find->next;
	}
	return (NULL);
}

t_list_r	*ft_copy_list(t_list_r *src)
{
	t_list_r	*new;
	t_list_r	*point;

	((new = (t_list_r*)malloc(sizeof(t_list_r))) == NULL) ? ft_error(2) : 0;
	((new->name = ft_strdup(src->name)) == NULL) ? ft_error(2) : 0;
	new->x = src->x;
	new->y = src->y;
	new->locked = 0;
	new->next = NULL;
	src = src->next;
	point = new;
	while (src)
	{
		((point->next = (t_list_r*)malloc(sizeof(t_list_r))) == NULL) ?
			ft_error(2) : 0;
		point = point->next;
		((point->name = ft_strdup(src->name)) == NULL) ? ft_error(2) : 0;
		point->x = src->x;
		point->y = src->y;
		point->locked = 0;
		point->next = NULL;
		src = src->next;
	}
	return (new);
}

t_list_r	*ft_copy_room(t_list_r *src)
{
	t_list_r	*new;

	((new = (t_list_r*)malloc(sizeof(t_list_r))) == NULL) ? ft_error(2) : 0;
	((new->name = ft_strdup(src->name)) == NULL) ? ft_error(2) : 0;
	new->x = src->x;
	new->y = src->y;
	new->start_end = src->start_end;
	new->locked = 0;
	new->next = NULL;
	return (new);
}
