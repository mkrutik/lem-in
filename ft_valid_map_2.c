/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_valid_map_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 15:13:25 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 09:55:26 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

int		ft_atoi_ssize(char *src)
{
	ssize_t		tmp;
	ssize_t		sign;

	tmp = 0;
	sign = 1;
	while (ft_isspace(*src))
		src++;
	(*src == '-') ? (sign = -1) : 0;
	(*src == '-' || *src == '+') ? (src++) : 0;
	while (ft_isdigit(*src))
	{
		tmp *= 10;
		tmp += *src - '0';
		src++;
	}
	(*src != '\0' && *src != '-' && !ft_isdigit(*src)) ? ft_error(3) : 0;
	tmp = tmp * sign;
	(tmp > 2147483647 || tmp < 1) ? ft_error(3) : 0;
	return ((int)tmp);
}

int		ft_valid_first_line(char *src)
{
	int		i;

	i = 0;
	(!ft_isdigit(src[i]) || ft_strlen(src) > 10) ? ft_error(3) : 0;
	while (ft_isdigit(src[i]))
		i++;
	(src[i] != '\0') ? ft_error(3) : 0;
	return (1);
}

int		ft_valid_room(char *src, int i, int name, int x)
{
	int		y;

	(src[0] == '#' || src[0] == 'L') ? ft_error(9) : 0;
	while (ft_isspace(src[i]))
		i++;
	name = (src[i] != '\0') ? 1 : 0;
	while (!ft_isspace(src[i]) && src[i] != '\0')
		i++;
	while (ft_isspace(src[i]))
		i++;
	x = (src[i] != '\0') ? 1 : 0;
	while (ft_isdigit(src[i]))
		i++;
	while (ft_isspace(src[i]))
		i++;
	y = (src[i] != '\0') ? 1 : 0;
	while (src[i] && ft_isdigit(src[i]))
		i++;
	while (ft_isspace(src[i]))
		i++;
	(src[i] != '\0' || name == 0 || x == 0 || y == 0) ? ft_error(9) : 0;
	return (1);
}

int		ft_is_link(char *src, int i, int space, int f)
{
	while (ft_isspace(src[i]))
	{
		i++;
		space++;
		f++;
	}
	while (src[i])
	{
		if (ft_isspace(src[i]))
		{
			((i - 1) != 0 && src[i - 1] == '-' && ft_isspace(src[i])) ? (f++) :
				0;
			while (ft_isspace(src[i]))
				i++;
			space++;
		}
		(src[i] != '\0') ? (i++) : 0;
	}
	if (f != space || f > 2)
		return (0);
	return (1);
}

int		ft_find_name_in_list(t_list_r *head, char *src)
{
	t_list_r	*point;

	point = head;
	while (point)
	{
		if (point->name != NULL && ft_strcmp(point->name, src) == 0)
			return (1);
		point = point->next;
	}
	return (0);
}
