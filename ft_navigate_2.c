/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_navigate_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 10:31:42 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 10:33:39 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

int		ft_check_ant(t_ant **head, t_data *src)
{
	ssize_t	i;

	i = 0;
	while (i < src->ant)
	{
		if (head[i]->home == NULL || ft_strcmp(head[i]->home, src->n2))
			return (1);
		i++;
	}
	return (0);
}

int		ft_is_room_loked(t_list_r *head, char *name)
{
	t_list_r	*point;

	point = head;
	while (point)
	{
		if (!ft_strcmp(name, point->name))
		{
			if (point->start_end == 2 || point->locked == 0)
				return (0);
			return (1);
		}
		point = point->next;
	}
	return (0);
}

void	ft_lock_or_unlock(t_list_r *head, char *unlock, char *lock)
{
	t_list_r	*point;

	point = head;
	while (point)
	{
		if (unlock != NULL && !ft_strcmp(point->name, unlock))
			point->locked = 0;
		if (lock != NULL && !ft_strcmp(point->name, lock))
			point->locked = 1;
		point = point->next;
	}
}
