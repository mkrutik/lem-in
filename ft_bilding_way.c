/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bilding_way.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 11:44:05 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 11:44:08 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

int		ft_is_origin_r(t_list_r *head, char *name)
{
	t_list_r	*point;

	point = head;
	while (point)
	{
		if (ft_strcmp(point->name, name) == 0)
			return (1);
		point = point->next;
	}
	return (0);
}

void	ft_add_new_way(t_data *src, t_list_r *way, char *name)
{
	t_list_r	*point;
	t_list_r	*end;
	t_stroke	*p;
	int			f;

	p = src->way;
	f = 0;
	while (p->next)
		(p->next) ? (f++) && (p = p->next) : 0;
	((p->next = (t_stroke*)malloc(sizeof(t_stroke))) == NULL) ? ft_error(2) : 0;
	p->next->next = NULL;
	p->next->f = ++f;
	p->next->n_op = 0;
	p->next->head = ft_copy_list(way);
	point = p->next->head;
	while (point->next)
		point = point->next;
	end = ft_find_room(src->list_room, name, 0);
	free(point->name);
	((point->name = ft_strdup(end->name)) == NULL) ? ft_error(2) : 0;
	point->x = end->x;
	point->y = end->y;
	point->start_end = end->start_end;
	point->next = NULL;
	point->locked = 0;
}

void	ft_new_empty_way(t_data *src, char *name)
{
	t_stroke	*new;
	t_stroke	*point;
	t_list_r	*room;
	int			f;

	point = src->way;
	f = 0;
	while (point->next)
	{
		f++;
		point = point->next;
	}
	((new = (t_stroke*)malloc(sizeof(t_stroke))) == NULL) ? ft_error(2) : 0;
	room = ft_find_room(src->list_room, name, 0);
	new->n_op = 0;
	new->next = NULL;
	new->head = ft_copy_room(room);
	point->next = new;
	new->f = ++f;
}

void	ft_bild_branch(t_stroke *src, t_data *data, char *n)
{
	t_list_r	*point;

	point = src->head;
	while (point->next)
		point = point->next;
	while (1)
	{
		if (point->name == NULL)
			ft_bild_first_room(point, data, n);
		else
		{
			point = ft_bild_another_way(src, point, data);
			if (point == NULL)
				return ;
		}
		(point->start_end == 2) ? (data->n2 = point->name) : 0;
		if (point->start_end == 2)
			return ;
	}
}

void	ft_bild_tre(t_data *src)
{
	int			i;
	t_list_r	*start;
	t_stroke	*point;

	ft_find_dublicate_coordinates(src->list_room, src->room, 0);
	ft_find_dublicate_link(src->list_link, src->links, 0);
	start = ft_find_room(src->list_room, NULL, 1);
	((i = ft_count_rooms(src, start->name)) == 0) ? ft_error(11) : 0;
	((src->way = (t_stroke*)malloc(sizeof(t_stroke))) == NULL) ?
		ft_error(2) : 0;
	src->way->next = NULL;
	src->way->f = 0;
	((src->way->head = (t_list_r*)malloc(sizeof(t_list_r))) == NULL) ?
		ft_error(2) : 0;
	src->way->head->name = NULL;
	point = src->way;
	while (point)
	{
		ft_bild_branch(point, src, NULL);
		point = point->next;
	}
	ft_validate_ways(&src->way);
	(src->way == NULL) ? ft_error(11) : 0;
	ft_sort_ways(src->way, 0, 0, 1);
	ft_create_ant_list(src);
}
