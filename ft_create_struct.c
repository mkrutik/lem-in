/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_struct.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/01 15:22:45 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 09:42:41 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_cut_array(char *src, char **name, int *x, int *y)
{
	int		i;
	int		end;
	char	*tmp;

	i = (ft_isspace(src[0])) ? 1 : 0;
	while (!ft_isspace(src[i]) && src[i] != '\0')
		i++;
	*name = ft_strsub(src, 0, i);
	while (ft_isspace(src[i]))
		i++;
	end = i;
	while (!ft_isspace(src[end]) && src[i] != '\0')
		end++;
	tmp = ft_strsub(src, i, (end - i));
	*x = ft_atoi(tmp);
	free(tmp);
	while (ft_isspace(src[end]))
		end++;
	i = end;
	while (!ft_isspace(src[end]) && src[end] != '\0')
		end++;
	tmp = ft_strsub(src, i, (end - i));
	*y = ft_atoi(tmp);
	free(tmp);
}

void	ft_create_list_room(t_list_r *head, char *src, int f)
{
	t_list_r		*point;

	if (head->name == NULL)
	{
		ft_cut_array(src, &head->name, &head->x, &head->y);
		(f == 1) ? head->start_end = 1 : 0;
		(f == 2) ? head->start_end = 2 : 0;
		(f == 0) ? head->start_end = 0 : 0;
		head->locked = 0;
		return ;
	}
	point = head;
	while (point->next)
	{
		(ft_strcmp(point->name, src) == 0) ? ft_error(9) : 0;
		point = point->next;
	}
	((point->next = (t_list_r*)malloc(sizeof(t_list_r))) == NULL) ? ft_error(2)
		: 0;
	ft_cut_array(src, &point->next->name, &point->next->x, &point->next->y);
	(f == 1) ? point->next->start_end = 1 : 0;
	(f == 2) ? point->next->start_end = 2 : 0;
	(f == 0) ? point->next->start_end = 0 : 0;
	point->next->locked = 0;
	point->next->next = NULL;
}

void	ft_create_link(t_list_link *head, char *name1, char *name2)
{
	t_list_link	*point;

	if (head->name1 == NULL)
	{
		((head->name1 = ft_strdup(name1)) == NULL) ? ft_error(2) : 0;
		((head->name2 = ft_strdup(name2)) == NULL) ? ft_error(2) : 0;
		head->next = NULL;
	}
	else
	{
		point = head;
		while (point->next)
			point = point->next;
		((point->next = (t_list_link*)malloc(sizeof(t_list_link))) == NULL) ?
			ft_error(2) : 0;
		((point->next->name1 = ft_strdup(name1)) == NULL) ? ft_error(2) : 0;
		((point->next->name2 = ft_strdup(name2)) == NULL) ? ft_error(2) : 0;
		point->next->next = NULL;
	}
	free(name1);
	free(name2);
}

void	ft_create_ant_list(t_data *src)
{
	int i;

	((src->list_ant = (t_ant**)malloc(sizeof(t_ant*) * src->ant)) == NULL) ?
		ft_error(2) : 0;
	i = 0;
	while (i < src->ant)
	{
		((src->list_ant[i] = (t_ant*)malloc(sizeof(t_ant))) == NULL) ?
			ft_error(2) : 0;
		src->list_ant[i]->n = i + 1;
		src->list_ant[i]->way = -1;
		src->list_ant[i]->home = NULL;
		++i;
	}
	return ;
}
