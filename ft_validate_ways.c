/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_validate_ways.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/01 09:43:44 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 09:58:30 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_del_way_list(t_list_r *head)
{
	t_list_r	*del;

	while (head)
	{
		del = head;
		head = head->next;
		free(del->name);
		free(del);
	}
}

int		ft_valid_or_not(t_list_r *head)
{
	t_list_r	*point;
	int			i;

	point = head;
	i = 1;
	while (point->next)
	{
		i++;
		point = point->next;
	}
	if (point->start_end != 2)
		return (0);
	return (i);
}

void	ft_validate_ways(t_stroke **src)
{
	int		i;

	if ((*src)->next)
		ft_validate_ways(&(*src)->next);
	if ((i = ft_valid_or_not((*src)->head)) == 0)
	{
		ft_del_way_list((*src)->head);
		free(*(src));
		*src = (*src)->next;
	}
	else
		(*src)->n_op = i;
	return ;
}

void	ft_sort_ways(t_stroke *head, int tmp_op, int tmp_f, int swapped)
{
	t_stroke		*point;
	t_list_r		*tmp;

	while (swapped--)
	{
		point = head;
		while (point->next)
		{
			if (point->n_op > point->next->n_op)
			{
				tmp = point->head;
				tmp_op = point->n_op;
				tmp_f = point->f;
				point->head = point->next->head;
				point->n_op = point->next->n_op;
				point->f = point->next->f;
				point->next->head = tmp;
				point->next->n_op = tmp_op;
				point->next->f = tmp_f;
				swapped = 1;
			}
			else
				point = point->next;
		}
	}
}
