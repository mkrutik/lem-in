/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lem_in.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 16:59:13 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 12:22:09 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LEM_IN_H
# define FT_LEM_IN_H
# include "get_next_line.h"
# include "libft/libft.h"

typedef struct			s_list_r
{
	char				*name;
	int					start_end;
	int					x;
	int					y;
	int					locked;
	struct s_list_r		*next;
}						t_list_r;

typedef struct			s_link
{
	char				*name1;
	char				*name2;
	struct s_link		*next;
}						t_list_link;

typedef struct			s_ant
{
	int					n;
	char				*home;
	int					way;
}						t_ant;

typedef struct			s_stroke
{
	int					n_op;
	int					f;
	t_list_r			*head;
	struct s_stroke		*next;
}						t_stroke;

typedef struct			s_data
{
	int					ant;
	unsigned int		room;
	int					start;
	int					end;
	int					links;
	char				**map;
	t_list_r			*list_room;
	t_list_link			*list_link;
	t_ant				**list_ant;
	char				*n1;
	char				*n2;
	t_stroke			*way;
	int					color;
}						t_data;

void					ft_error(int num_error);
void					ft_valid(t_data *src, int i);
void					ft_create_list_room(t_list_r *head, char *src, int f);
void					ft_create_link(t_list_link *head, char *name1,
							char *name2);
int						ft_valid_first_line(char *src);
int						ft_valid_room(char *src, int i, int name, int x);
int						ft_is_link(char *src, int i, int space, int f);
int						ft_find_name_in_list(t_list_r *head, char *src);
int						ft_atoi_ssize(char *src);
void					ft_bild_tre(t_data *src);
void					ft_find_dublicate_link(t_list_link *head, int l, int i);
void					ft_find_dublicate_coordinates(t_list_r *head,
							unsigned int len, int i);
t_list_r				*ft_copy_room(t_list_r *src);
t_list_r				*ft_find_room(t_list_r *head, char *name, int n);
int						ft_count_rooms(t_data *src, char *name);
char					*ft_find_link(t_list_link *head, char *name, int i);
t_list_r				*ft_copy_list(t_list_r *src);
t_list_r				*ft_copy_room(t_list_r *src);
void					ft_validate_ways(t_stroke **src);
void					ft_sort_ways(t_stroke *head, int op, int f, int s);
void					ft_create_ant_list(t_data *src);
void					ft_navigate(t_data *data);
int						ft_check_ant(t_ant **s, t_data *src);
int						ft_is_room_loked(t_list_r *h, char *name);
void					ft_lock_or_unlock(t_list_r *h, char *u, char *lock);
void					ft_bild_first_room(t_list_r *p, t_data *s, char *n);
void					ft_new_empty_way(t_data *src, char *name);
t_list_r				*ft_bild_another_way(t_stroke *s, t_list_r *p,
							t_data *d);
int						ft_is_origin_r(t_list_r *s, char *name);
void					ft_add_new_way(t_data *d, t_list_r *s, char *name);
#endif
