/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 15:14:05 by mkrutik           #+#    #+#             */
/*   Updated: 2017/04/02 10:00:22 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lem_in.h"

void	ft_error(int n)
{
	ft_putstr("\033[0;31m");
	ft_putstr("ERROR\n");
	(n == 1 || n == 3 || n == 4 || n == 5 || n == 6 || n == 7 || n == 8 ||
	n == 9 || n == 10) ? ft_putstr("MAP file doesn`t valid\n") : 0;
	(n == 2) ? ft_putstr("Memory allocation error !!!\n") : 0;
	(n == 3) ? ft_putstr("THERE IS NO ANTs OR THE FIRST LINE ISNT VALID\n") : 0;
	(n == 4) ? ft_putstr("THERE IS NO ROOMs !!!\n") : 0;
	(n == 5) ? ft_putstr("THERE IS NO LINKs !!!\n") : 0;
	(n == 6) ? ft_putstr("THERE IS NO START !!!\n") : 0;
	(n == 7) ? ft_putstr("THERE IS NO END !!!\n") : 0;
	(n == 8) ? ft_putstr("MAP HAS ON OR MORE INVALID LINE\n") : 0;
	(n == 9) ? ft_putstr("ONE OR MORE ROOM IS NOT VALID\n") : 0;
	(n == 10) ? ft_putstr("ONE OR MORE LINK IS NOT VALID\n") : 0;
	(n == 11) ? ft_putstr("THERE IS NO POSSIBLE SOLUTION\n") : 0;
	ft_putstr("USAGE: ./lem-in <mapfile name>\n");
	exit(-1);
}
