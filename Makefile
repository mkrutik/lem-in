# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/02 11:47:47 by mkrutik           #+#    #+#              #
#    Updated: 2017/04/02 12:22:16 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in

SRC = ft_del_dublicate_link.c \
	  ft_navigate_2.c \
	  ft_bild_way_2.c \
	  ft_error.c \
	  ft_valid_map.c \
	  main.c \
	  ft_bilding_way.c \
	  ft_find_dublicate_coordinates.c \
	  ft_valid_map_2.c \
	  ft_bilding_way_2.c \
	  ft_validate_ways.c \
	  ft_create_struct.c \
	  ft_navigate.c	\
	  get_next_line.c

OBJ = $(SRC:.c=.o)

GFLAGS = -Wall -Wextra -Werror

LIBINC = -I libft/libft.h -L./libft -lft

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft/
	gcc $(GFLAGS) $(OBJ) -o $(NAME) $(LIBINC)

clean:
	rm -f $(OBJ)
	make clean -C libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C libft/

re: fclean all

